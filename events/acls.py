from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import requests


def get_picture_url(city, state):
    header = {"Authorization": PEXELS_API_KEY}
    query = {f"{city} {state}"}
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, headers=header, params={"query": query})
    if response.json():
        picture_url = response.json()["photos"][0]["src"]["original"]
    else:
        picture_url = None
    return {"picture_url": picture_url}


def get_lat_lon(city, state):
    query = {
        "q": f"{city}, {state}, US",
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"

    response = requests.get(url, params=query)
    if response.json():
        lat = response.json()[0]["lat"]
        lon = response.json()[0]["lon"]
    else:
        return None

    return {"lat": lat, "lon": lon}


def get_weather_data(city, state):
    lat_lon = get_lat_lon(city, state)
    if lat_lon is None:
        return None

    query = {
        "lat": lat_lon["lat"],
        "lon": lat_lon["lon"],
        "units": "imperial",
        "appid": OPEN_WEATHER_API_KEY,
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=query)

    temp = response.json()["main"]["temp"]
    description = response.json()["weather"][0]["description"]
    return {"temp": temp, "description": description}
